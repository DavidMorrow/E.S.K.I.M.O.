# Projekt E.S.K.I.M.O. 

![Eskimo_Logo](Images/eskimo_logo_small.png)

**E**infache **S**martmirror **K**onfiguration und **I**nstallation **M**ittels **O**berfläche

Projekt ESKIMO wird im Rahmen einer Masterthesis an der Hochschule Bremen während des Sommersemesters 2021 im Studiengang M.Sc. Informatik entwickelt und bietet die Möglichkeit, einen [Smart Mirror](https://de.wikipedia.org/wiki/Smart_Mirror) zu Installieren und zu Konfigurieren. Zur Erleichterung der Installation stellt ESKIMO eine grafische Oberfläche ([GUI](https://it-talents.de/it-wissen/gui/)) zur Verfügung.

[![E.S.K.I.M.O. Trailer](/Images/Thumbnail_yt_small.jpg)](https://www.youtube.com/watch?v=lOzYo7cDUOo)

## Funktionsumfang

Funktionsumfang nach Installationsroutine (Standard):
* Anzeige des Datums und der aktuellen Uhrzeit
* Anzeige eines Kalenders mit den Feriendaten der USA
* Anzeige des Wetters und der Wettervorschau für New York
* Anzeige von ermutigenden Sätzen (5 unterschiedliche Sätze in englischer Sprache)
* Anzeige der Nachrichten der New York Times

Optional sind folgende Features konfigurierbar:
* Anzeige eines eigenen Kalenders (Onlinekalender vorausgesetzt -> Einrichtung wird in der Installatiosnroutine erläutert)
* Anzeige des Wetters der Heimatstadt (API-Key vorausgesetzt -> Einrichtung wird in der Installationsroutine erläutert)
* Anzeige des favorisierten Nachrichten-Feeds (RSS-Feed-URL vorausgesetzt -> Bezug wird in der Installationsroutine erläutert)
* Anzeige von ermutigenden Sätzen (X beliebige Sätze die selbst verfasst werden können)
* Neustart des Smart Mirrors nach der Installation
* Bei Systemstart die Smart Mirror-Software ausführen
* Eine Verknüpfung zum Starten des Smart Mirrors auf dem Desktop einrichten
* Den Smart Mirror direkt nach der Installation starten
Die Punkte "Neustart des Gerätes" und "Direkter Start des Smart Mirrors" können nicht beide zusammen ausgewählt werden, sodass der Benutzer sich für eine option entscheiden muss. Zum ausprobieren gibt es in Files/Testdaten.txt ein Dokument mit Mockdaten.

Als Kernapplikation für den Smart Mirror wird die Software [MagicMirror²](https://magicmirror.builders/) installiert.

## Voraussetzungen

Folgende Voraussetzungen müssen gegeben sein, um mit ESKIMO einen Smart Mirror installieren und konfigurieren zu können.

### Hardware

**Raspberry Pi 4B**

Ein [Raspberry Pi 4B](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/?resellerType=home) dient als Hauptcomputer für das Projekt. Auf ihm wird ESKIMO ausgeführt, um weitere Software zu installieren.

**Micro-SD-Karte**

Um ein Betriebssystem auf dem Raspberry Pi 4B zu installieren, dient eine [Micro-SD-Karte](https://www.amazon.de/gp/product/B07FCMBLV6/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1) als Medium.

**Stromversorgung des Raspberry Pis**

Da der Raspberry Pi 4B nicht mit einer Stromversorgung geliefert wird, bietet sich entweder eine Powerbank oder ein extra Netzteil zur dauerhaften Stromversorgung an. Für das Masterprojekt wurde dieses [Netzteil](https://www.amazon.de/gp/product/B07TYYV8GF/ref=ppx_yo_dt_b_asin_title_o02_s01?ie=UTF8&psc=1) verwendet.

**HDMI zu MicroHDMI Kabel**

Da der Raspberry Pi 4B nur Bildschirme über micro-HDMI zulässt, muss ein [HDMI zu micro-HDMI Kabel](https://www.amazon.de/Snowkids-zukunftssicheres-TV-Kabel-unterst%C3%BCtzt-ARC-f%C3%A4hig-Grau/dp/B07Z838XB3/ref=sr_1_1_sspa?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=hdmi+zu+micro+hdmi&qid=1605403561&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUE2VDJOWDFYQllLWDMmZW5jcnlwdGVkSWQ9QTA0NDExOTQyNFFRU1ZXTlNEUVFRJmVuY3J5cHRlZEFkSWQ9QTA0OTMwMjBWOFVCTk1CMUdSRjYmd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl) bei der Übertragung des Signals eingesetzt werden.

**Monitor/Fernseher mit HDMI-Anschluss**

Der Monitor der am Raspberry Pi angeschlossen wird, dient als Anzeigemedium. Im Masterprojekt wurde dieser [Monitor](https://www.amazon.de/DYON-Fernseher-Triple-Netflix-Modelljahr/dp/B08552FMQP/ref=sr_1_4?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=amazon+35+zoll+hdmi&qid=1605403256&sr=8-4) verwendet. Dieser muss keine Anforderungen bis auf den Anschluss über HDMI erfüllen. Der Monitor sollte jedoch mindestens eine Auflösung von 1020 x 560 px entsprechen, um die GUI korrekt anzeigen zu können.

**Spionspiegel**

Ein [Spionspiegel](https://www.glas-star.de/collections/mirropane-chrome-spy) dient vor dem Monitor als lichtdurchlässige Oberfläche für den Smart Mirror und als Spiegel.

**Rahmen**

Holz wird genutzt um einen passenden Rahmen für den Smart Mirror zu bauen. Dieses kann ebenso wie Dübel, Schrauben und Farbe zur Verschönerung des Rahmens bei einem lokalen Baumarkt nach belieben bezogen werden.

**Maus und Tastatur**

Für die Installation und Konfiguration des Raspberry Pi 4B werden eine [Maus und Tastatur](https://www.amazon.de/AmazonBasics-kabelloser-Tastatur-kompakt-Layout/dp/B0787HRQN4/ref=sr_1_3?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=amazon+basics+tastatur+wireless+maus&qid=1605404557&sr=8-3) benötigt.

**MicroSD-Kartenleser**

Um das Betriebssystem [Raspberry Pi OS](https://www.raspberrypi.org/software/) von einem anderen PC auf die microSD-Karte zu flashen wird ein Kartenleser verwendet. Für das Projekt wurde eine [Speicherkartenadapter](https://www.amazon.de/Vanja-Kartenleser-Adapter-Speicherkartenleser-RS-MMC/dp/B00W02VHM6/ref=sr_1_2_sspa?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=usb+micro+sd+adapter&qid=1605404710&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzRkpRWVEzQU9XU0FCJmVuY3J5cHRlZElkPUEwNTEzODI3SE8yMkhCTzlGT1hPJmVuY3J5cHRlZEFkSWQ9QTA0MjY0NTgzOThTTjNIMFBDWDRZJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==) über USB genutzt.

---

### Software auf dem Raspberry Pi

Jegliche Software die für dieses Projekt genutzt wurde, ist kostenlos beziehbar!

**Raspbian OS**

Als Betriebssystem das [Raspberry Pi OS](https://www.raspberrypi.org/software/) in der version 5.4 vom 20.08.2020, welches das offizielle Betriebssystem des Herstellers des Raspberry Pi 4B ist. Für ESKIMO muss eine grafische Oberfläche vorhanden sein, weswegen die Version _Raspberry Pi OS with desktop and recommended software_ genutzt wurde. Ein weiterer Vorteil dieser Version ist, dass alle weiteren hier im  Software Abschnitt beschriebenen Programme bereits installiert sind.

**Git**

[Git](https://git-scm.com/about) ist ein Versionsverwaltungstool. Git wird genutzt, um ESKIMO und die Kernapplikation MagicMirror² auf den Raspberry Pi herunterzuladen.

**NodeJS**

[NodeJS](https://nodejs.org/de/) ist eine JavaScript-Laufzeitumgebung die dazu genutzt wird, serverbasierte Webanwendungen in JavaScript außerhalb von Webbrowsern auszuführen. Es wird als Voraussetzung für die Installation von MagicMirror² genannt.

**NPM**

[NPM ](https://www.npmjs.com/) ist eine Paketmanager Software für NodeJS. Sie wird benötigt, um den MagicMirror² zu starten.

**Java**

[Java](https://java.com/de/download/help/whatis_java.html) ist eine Programmiersprache und eine Laufzeitumgebung zugleich. Um Java-Applikationen auf einem Gerät ausführen zu können, muss Java installiert sein. ESKIMO ist eine Anwendung die in Java geschrieben wurde und benötigt somit auch die ihre Laufzeitumgebung.

**MagicMirror²**

 [MagicMirror²](https://magicmirror.builders/) ist eine OpenSource-Software, die Daten für den Bildschirm aufarbeitet und in einem Smart Mirror-Format darstellt. MagicMirror² muss nicht vorher installiert werden!

 **XScreensaver**

Da der Raspberry Pi nach einiger Zeit einen Bildschirmschoner anzeigt, wird empfohlen den Bildschirmschoner abzustellen. Dies ist bei dem Raspberry Pi nicht ohne Zusatzsoftware möglich. Eine Möglichkeit den Bildschirmschoner abzustellen bietet XScreensaver. Eine Installationsmethode wird in diesem [Thread](https://stackoverflow.com/questions/30985964/how-to-disable-sleeping-on-raspberry-pi) genannt.


---

### Konfigurationen

**Format des Bildschirms**

Wird der Monitor quer oder hochkant als Smart Mirror genutzt, muss dies im Vorfeld eingestellt werden. Dies kann mit dem Befehl _xrandr_ durchgeführt werden. Eine Anleitung befindet sich [hier](https://www.linuxmintusers.de/index.php?PHPSESSID=4207fda315dec715b94151c21a660830&topic=2126.msg14075#msg14075). Zudem sollte eine möglichst hohe Auflösung eingestellt werden.

**Konfigurationen am Monitor**

Bei manchen Monitoren müssen ggf. Einstellungen vorgenommen werden, damit der Raspberry Pi ein Bild anzeigen kann. Dazu lesen Sie bitte in der Installationsanleitung Ihres Monitors nach, welche Einstellungen vorgenommen werden müssen und führen diese über [PuTTy](https://www.putty.org/) oder durch das anschließen eines anderen Monitors durch. Eine Anleitung zur Installation über PuTTy finden Sie [hier](https://tutorials-raspberrypi.de/raspberry-pi-ssh-windows-zugriff-putty/).


---
# Step-by-Step Anleitung

In den folgenden Punkten wird erläutert, was alles vor der Nutzung von ESKIMO gegeben sein muss.

## 1. Auf- und Zusammenbau des Smart Mirrors inkl. Raspberry Pi

Wie der Smart Mirror zusammengebaut wird kann bei [Youtube](https://www.youtube.com/watch?v=aa3VVZA0e5Y) oder in [diesem Thread](https://winheim.net/2019/06/09/magicmirror-die-perfekte-ergaenzung-in-jedem-smart-home/) nachgelesen werden. Im genannten Thread kann auch die Durchführung einer herkömmlichen Installation von MagicMirror² ohne ESKIMO betrachtet werden, welche aber durch ESKIMO vereinfacht werden soll.

## 2. Vorbereitung des Raspberry Pi's für die Nutzung von ESKIMO

**Raspbian auf den Raspberry Pi 4B flashen**

Um das Raspbian OS auf die o.g. microSD-Karte zu spielen, wird ein PC mit Windows 10, und der o.g. Speicherkartenadapter genutzt. Das [HP USB Disk Storage Format Tool](https://www.heise.de/download/product/hp-usb-disk-storage-format-tool-97463) in der Version 2.2.3 wurde genutzt, um die microSD-Karte in das vom Raspbian OS geforderte FAT32 System zu formatieren. Anschließend wurde das o.g. Raspbian OS heruntergeladen und mit Hilfe des Tools [Win32DiskImager ](https://www.heise.de/download/product/win32-disk-imager-92033)der Version 1.0.0 auf die microSD-Karte gespielt.

**Überprüfung der Softwarepakete auf dem Raspberry Pi**

Nach erfolgreicher Installation des Raspbian OS, sollte der Raspberry Pi nun booten, eine grafische Oberfläche und den ersten Konfigurationsdialog anzeigen. Wichtig dabei ist, dass eine Internetverbindung eingerichtet wird (über Kabel oder WLAN - wobei WLAN für eine Smart Mirror vorteilhafter ist hinsichtlich der zu verlegenden Kabel) Über STRG-ALT-T kann nun ein [Linux Terminal](https://wiki.ubuntuusers.de/Terminal/) geöffnet werden. Über die folgenden Terminal-Befehle kann geprüft werden, ob und in welcher Version die benötigten Tools vorhanden sind:

`git --version`

`node --version`

`npm --version`

`java --version`


Haben alle Befehle eine Version zurück gegeben, so kann mit dem nächsten Schritt begonnen werden. Sollte eine Software nicht installiert sein,
so kann die aktuelleste Software für den Raspberry Pi mit den folgenden Befehlen heruntergeladen und installiert werden:

`sudo apt update`

`sudo apt-get install git`

`sudo apt-get install default-jre`

`sudo apt-get install npm`

`sudo apt-get install nodejs`

Danach sollte mit den o.g. Versionsabfragebefehlen geprüft werden, ob Raspbian die Installation(en) erfolgreich durchgeführt hat.

## 3. Installation des Smart Mirorrs über ESKIMO

Um ESKIMO zur Installation des Smart Mirrors zu nutzen, muss das Tool von GitLab heruntergeladen werden. Dazu muss ein Linux-Terminal mit den Tasten (STRG-ALT-T) aufgerufen und folgende Befehlskette eingegeben werden:

`cd /home/pi/` 
(Wechseln in das Verzeichnis _/home/pi/_)

`sudo git clone https://gitlab.com/DavidMorrow/E.S.K.I.M.O..git`
(Herunterladen von ESKIMO)

`cd E.S.K.I.M.O.`
(Wechseln in den ESKIMO Ordner _/home/pi/E.S.K.I.M.O._)

`sudo chmod +x Start-ESKIMO.sh`
(Das Start-ESKIMO.sh-Skript ausführbar machen)

Anschließend navigiert man durch den Explorer (Ordner Symbol in der oberen Taskleiste von Linux) in das Verzeichnis _/home/pi/E.S.K.I.M.O._ und kann das Start-ESKIMO.sh-Skript über einen Doppelklick aufrufen.


Alle weiteren Schritte werden in ESKIMO erläutert!

Viel Spaß!

---

Autor & Entwickler: David Morrow

Kontakt: dmorrow@stud.hs-bremen.de
