# Deinstall Old Installations
sudo rm -rf /home/pi/MagicMirror
sudo rm /home/pi/MagicMirror/MagicMirror.sh
sudo rm /home/pi/Desktop/MagicMirror.sh

# Delete Autostart of MagicMirror
line=$(cat /etc/rc.local | grep -n "exit 0" | tail -1 | cut -f1 -d:)
insertline=$((line-1))
linecontains=$(sed "${insertline}q;d" /etc/rc.local)
if [ ! -z "$linecontains" ]
then
sudo sed -i "${insertline}s/.*//" /etc/rc.local
fi

# Make new directory and Clone the original Files
mkdir /home/pi/MagicMirror
git clone https://github.com/MichMich/MagicMirror /home/pi/MagicMirror
cd /home/pi/MagicMirror

# Install MagicMirror
npm install

# Copy Config
cp /home/pi/MagicMirror/config/config.js.sample /home/pi/MagicMirror/config/config.js